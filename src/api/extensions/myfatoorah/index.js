import { apiStatus } from '../../../lib/util';
import { Router } from 'express';
const request = require('request')

module.exports = ({ config, db }) => {
  let mcApi = Router();

  /**
   * POST Initiatise Payment
   */
  mcApi.post('/initiatePayment', (req, res) => {
    let carData = req.body
    if (!carData.InvoiceAmount) {
      apiStatus(res, 'Invalid cart data provided!', 400)
      return
    }

    let url = config.extensions.myfatoorah.apiUrl + '/v2/InitiatePayment'
    request({
      url: url,
      method: 'POST',
      headers: {
        'Authorization': 'bearer ' + config.extensions.myfatoorah.apiKey,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      json: true,
      body: carData
    }, (error, response, body) => {
      if (error || response.statusCode !== 200) {
        apiStatus(res, 'An error occured while accessing MyFatoorah', 500)
      } else {
        apiStatus(res, body, 200)
      }
    })
  })

  /**
   * POST Execute Payment
   */
  mcApi.post('/executePayment', (req, res) => {
    let carData = req.body
    if (!carData.PaymentMethodId) {
      apiStatus(res, 'Invalid cart data provided!', 400)
      return
    }

    let url = config.extensions.myfatoorah.apiUrl + '/v2/ExecutePayment'
    request({
      url: url,
      method: 'POST',
      headers: {
        'Authorization': 'bearer ' + config.extensions.myfatoorah.apiKey,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      json: true,
      body: carData
    }, (error, response, body) => {
      if (error || response.statusCode !== 200) {
        apiStatus(res, 'An error occured while accessing MyFatoorah', 500)
      } else {
        apiStatus(res, body, 200)
      }
    })
  })

  /**
   * POST Execute Payment
   */
  mcApi.post('/directPayment', (req, res) => {
    let cardData = req.body
    if (!cardData.apiURL || !cardData.card) {
      apiStatus(res, 'Invalid cart data provided!', 400)
      return
    }

    let url = cardData.apiURL
    request({
      url: url,
      method: 'POST',
      headers: {
        'Authorization': 'bearer ' + config.extensions.myfatoorah.apiKey,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      json: true,
      body: cardData.card
    }, (error, response, body) => {
      if (error || response.statusCode !== 200) {
        apiStatus(res, 'An error occured while accessing MyFatoorah', 500)
      } else {
        apiStatus(res, body, 200)
      }
    })
  })

  return mcApi
}
